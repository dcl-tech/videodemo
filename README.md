# SDK7 Video Demo
by Carl Fravel
Discord:  Carl Fravel#3088

Demonstrates using 3D and 2D UIs to start, stop, and pause a video.
Note the SDK7 requirement to list the domain of the video source in scene.json,
for example, for the included video:

  "allowedMediaHostnames": [
    "player.vimeo.com"
  ],

# Known issues as of 9/9/2023

## 2D UI starting a video does noothing until you click in the scene
Pause and Stop work as expected

## The VideoPlayer.position is always at 0, and setting it to >0 has no effect



