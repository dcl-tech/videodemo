/////////////////////////////////////////////////////////
// SDK7 Scene to demo video
// (c) Carl Fravel
/////////////////////////////////////////////////////////

// Must export all the functions required to make the scene work, at least in alpha sdk7
// Without this, you get Error: Your scene does not export an onUpdate function. Documentation: https://dcl.gg/sdk/missing-onUpdate
export * from '@dcl/sdk'

import { 
  engine, 
  Entity,
  Material,
  Transform
} from '@dcl/sdk/ecs'

import {
  Vector3,
  Color4
} from '@dcl/sdk/math'

import { spawnEntity, spawnPlaneX } from './modules/SpawnerFunctions'
import {setupVideoDemo, SimpleVideoSource, VideoControls2D, VideoControls3D, VideoDemo} from './modules/VideoDemo'

let films:SimpleVideoSource[] = [
  {
    title: "DCL Demo",
    src:'https://player.vimeo.com/external/552481870.m3u8?s=c312c8533f97e808fccc92b0510b085c8122a875'
  }
]

const scene = engine.addEntity()
// Give the entity a position via a transform component
Transform.create(scene, { // todo is this unneeded, does it already have base Transform?
  position: Vector3.create(0, 0, 0)
})

export function main () {
  const screen = spawnPlaneX(8,1.77,15.9, 0,0,0,  5,2.925,1)
  const screenBacking = spawnPlaneX(8,1.77,15.95, 0,0,0,  5,2.925,1)
  // set initial color to black.  Video will replace this later.
  Material.setPbrMaterial(screen,{
    albedoColor: Color4.Black(),
    roughness:1
  })
  Material.setPbrMaterial(screenBacking,{
    albedoColor: Color4.Black(),
    roughness:1
  })
  Transform.getMutable(screen).parent = scene
  Transform.getMutable(screenBacking).parent = scene

  let simpleVideoControlsRoot = spawnEntity(8,0,5, 0,0,0, 1,1,1)
  let videoControls3D = new VideoControls3D(simpleVideoControlsRoot)
  let videoControls2D = new VideoControls2D()

  let videoDemo:VideoDemo = setupVideoDemo(films,[screen], videoControls3D, videoControls2D)
}

