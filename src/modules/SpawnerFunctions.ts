 //////////////////////////////////////////
// SpawnerFunctions
// (c) 2019, 2020 by Carl Fravel
// V2.0 Now adds an EntityName component to entities it creates, with an optional name param added to every spawner function. 
//      Backward compatible with previous version.

import { 
  engine,
  Entity,
  Transform,
  MeshRenderer,
  MeshCollider,
  GltfContainer,
  Schemas
} from "@dcl/sdk/ecs"

import {
  Quaternion,
  Vector3
} from "@dcl/sdk/math"

export const SpawnerNameComponent = engine.defineComponent(
	"SpawnerNameComponent",
	{
		name: Schemas.String
	})

  // import {MyNameComponent} from "./MyNameComponent"
  // MyNameComponent.create(e,{name:"testName"})


// @Component("EntityName")
// export class EntityName {
//   name:string=""
// }

export function spawnEntity(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
  // create the entity
  const entity = engine.addEntity()
  // set a transform to the entity
  Transform.create(entity, {
    parent:parent, 
    position: {x:x,y:y,z:z},
    rotation: Quaternion.fromEulerDegrees(rx,ry,rz),
    scale: {x:sx, y:sy, z:sz}
  })
  // todo sdk7 can we give the entity a name?   Custom component?
  // let c = new EntityName()
  // c.name = name
  // entity.addComponent(c)
  // add the entity to the engine

    if (name && name.length>0) {
      SpawnerNameComponent.create(entity,{name:name})
      let nameComponent = SpawnerNameComponent.get(entity)
      console.log("\n====== MyNameComponent.name = "+JSON.stringify(name))
    }
  return entity
}

export function spawnBoxX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name, parent)
  // set how the cube looks and collides
  MeshRenderer.setBox(entity)
  MeshCollider.setBox(entity)
  return entity
}

export function spawnBox(x: number, y: number, z: number, name: string = "", parent:Entity) {
  return spawnBoxX(x,y,z,0,0,0,1,1,1,name,parent)
}

/**
 * 
 * @param x scales the x dimension of the radius
 * @param y height of cylinder or cone //todo sdk7 has it be twice this height
 * @param z scales the y dimention of the radius    x and z must be equal for round cross section.
 * @param rx 
 * @param ry 
 * @param rz 
 * @param sx 
 * @param sy 
 * @param sz 
 * @param radiusBottom 
 * @param radiusTop 
 * @param name 
 * @param parent 
 * @returns 
 */
export function spawnCylinderX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number,  name: string = "", parent?:Entity, radiusBottom:number=0.5, radiusTop:number=0.5) {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy/2,sz,name, parent) // todo sdk7 cylinder is twice as high the input y, so for now cutting it in half
  // set a shape to the entity
  MeshRenderer.setCylinder(entity,radiusBottom,radiusTop)
  MeshCollider.setCylinder(entity,radiusBottom, radiusTop)
  return entity
}

export function spawnCylinder(x: number, y: number, z: number, name: string = "", parent?:Entity) {
  return spawnCylinderX(x,y,z,  0,0,0,  1,1,1, name,parent, 0.5, 0.5)
}


export function spawnConeX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy/2,sz,name,parent)
  // set a shape to the entity
  MeshRenderer.setCylinder(entity,0.5,0)
  MeshCollider.setCylinder(entity,0.5,0)
  return entity
}

export function spawnCone(x: number, y: number, z: number, name: string = "", parent?:Entity) {
  return spawnConeX(x,y,z, 0,0,0, 1,1,1 ,name,parent)
}




export function spawnGltfX(gltfPath: string, x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name, parent)
  GltfContainer.create(entity, { 
    src: gltfPath 
  })
  return entity
}


export function spawnGltf(gltfPath: string, x: number, y: number, z: number, name: string = "", parent?:Entity) {
  return spawnGltfX(gltfPath,x,y,z,0,0,0,1,1,1,name)
}

export function spawnPlaneX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name,parent)
  // set a shape to the entity
  MeshRenderer.setPlane(entity)
  MeshCollider.setPlane(entity)
  return entity
}

export function spawnPlane(x: number, y: number, z: number, name: string = "", parent?:Entity) {
  return spawnPlaneX(x,y,z,0,0,0,1,1,1,name)
}

export function spawnSphereX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name,parent)
  // set a shape to the entity
  MeshRenderer.setSphere(entity)
  MeshCollider.setSphere(entity)
  return entity
}

export function spawnSphere(x: number, y: number, z: number, name: string = "", parent?:Entity) {
  return spawnSphereX(x,y,z,0,0,0,1,1,1,name)
}

// // todo sdk7 TextShape
// export function spawnTextX(value: string, x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "", parent?:Entity) {
//   const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
//   // set a shape to the entity
//   entity.addComponent(new TextShape(value))
//   return entity
// }

// export function spawnText(value: string, x: number, y: number, z: number, name: string = "", parent?:Entity) {
//   return spawnTextX(value,x, y,z,0,0,0,1,1,1,name)
// }

