///////////////////////////////////////
// VideoDemo  
// Carl Fravel
///////////////////////////////////////

import { Color4, Quaternion } from '@dcl/sdk/math'
import { 
    engine,Entity,Transform,Material,
    TextShape, MeshCollider, MeshRenderer,
    pointerEventsSystem,InputAction, 
    VideoPlayer, PBVideoPlayer, TextureUnion
} from '@dcl/sdk/ecs'
import ReactEcs, { ReactEcsRenderer, UiEntity, Button, Label } from '@dcl/sdk/react-ecs'

export type SimpleVideoSource = {
	title:string
	src:string
}



enum VideoStates {
    STOPPED = "STOPPED",
    PAUSED = "PAUSED",
    PLAYING = "PLAYING"
}

const WIDTH = 242
const HEIGHT = 334
const POS_X = 0
const POS_Y = 199

const BUTTON_WIDTH = 29
const BUTTON_HEIGHT = 29

const COL_STOP = 123 // stop button
const COL_PAUSE = 90 // pause button
const COL_PLAY = 57 // play button
const COL_CAPTION = 10

const ROW_CAPTION = 295 // Caption at top of controls
const ROW_CATALOG = 57  // Bottom of catalog
const ROW_CONTROLS = 17  // Main controls row

const CAPTION_WIDTH = 100
const CAPTION_HEIGHT = 25
const CAPTION_FONT_SIZE = 18

const CATALOG_FRAME_X = 5
const CATALOG_FRAME_WIDTH = 194
const CATLOG_FRAME_HEIGHT = 228

const CATALOG_TEXT_X = 40
const CATALOG_TEXT_Y = 87
const CATALOG_TEXT_WIDTH = 140
const CATALOG_TEXT_HEIGHT = 188
const CATALOG_FONT_SIZE = 9

let theVideoDemo:VideoDemo // singleton instance global variable

function setupUi() {
    if (theVideoDemo && theVideoDemo.videoControls2D) {
        return <UiEntity //parent / modal decoration
        uiTransform={{
            positionType: 'absolute',
            width: WIDTH,
            height: HEIGHT,
            position: { right: POS_X, bottom: POS_Y },
        }}
        uiBackground={{
            color:Color4.Black()
        }}
    >
        <UiEntity // Caption at top of controls
            uiTransform={{ 
                positionType: 'absolute',
                width: CAPTION_WIDTH, 
                height: CAPTION_HEIGHT,
                position: { right: COL_CAPTION, bottom: ROW_CAPTION }
            }} >
            <Label
                value='Video Controls'
                color={Color4.White()}
                fontSize={CAPTION_FONT_SIZE}
                font="sans-serif"
                textAlign="middle-center" 
            />
        </UiEntity>
        <Button // catalog frame
            value=''
            onMouseDown={()=>{ 

            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: CATALOG_FRAME_WIDTH,
                height: CATLOG_FRAME_HEIGHT,
                position: { right: CATALOG_FRAME_X, bottom: ROW_CATALOG }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/stock-ui/blank.png"}, 
                textureMode:'stretch'
            }}
        >
        </Button>
        <UiEntity // Catalog Text
            uiTransform={{ 
                positionType: 'absolute',
                width: CATALOG_TEXT_WIDTH, 
                height: CATALOG_TEXT_HEIGHT,
                position: { right: CATALOG_TEXT_X, bottom: CATALOG_TEXT_Y } 
            }} >
            <Label
                value={((theVideoDemo.videoControls2D)?theVideoDemo.videoControls2D.getCatalogText():"UNDEFINED")}
                // color={Color4.White()}
                color={((theVideoDemo.videoControls2D)?theVideoDemo.videoControls2D.getCatalogTextColor():Color4.White())}
                fontSize={CATALOG_FONT_SIZE}
                font="sans-serif"
                textAlign="top-left"
            />
        </UiEntity>    
        <Button // Stop
            value=''
            onMouseDown={()=>{ 
                // theCinemaClient.stopCommand()
                theVideoDemo.stopVideo(true)
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL_STOP, bottom: ROW_CONTROLS }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/stock-ui/stop.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <Button // Pause
            value=''
            onMouseDown={()=>{ 
                // theCinemaClient.pauseCommand()
                theVideoDemo.pauseVideo()
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL_PAUSE, bottom: ROW_CONTROLS }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/stock-ui/pause.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
        <Button // Play
            value=''
            onMouseDown={()=>{ 
                //theCinemaClient.playCommand(theCinemaClient.videoControls.getCatalogDisplayIndex())
                theVideoDemo.playVideo()
            }}
            color={Color4.fromHexString("#FFFFFF00")} // to hide the text
            uiTransform={{
                positionType: 'absolute',
                width: BUTTON_WIDTH,
                height: BUTTON_HEIGHT,
                position: { right: COL_PLAY, bottom: ROW_CONTROLS }
            }}
            uiBackground={{
                color:Color4.White(),
                texture: {src: "materials/stock-ui/right.png"}, 
                textureMode:'stretch'
            }}
        ></Button>
    </UiEntity>        
    }
    else {
        return <UiEntity/>
    }
    
}

export class VideoControls2D {
  catalogDisplayIndex:number = 0
  maximizedLabel:string="Uninitialized"
  captionLabelText:string="Uninitialized"
  catalogText:string="Uninitialized"
  catalogTextColor:Color4 = Color4.White()
  videoDemo?:VideoDemo
  
  constructor () {
    ReactEcsRenderer.setUiRenderer(setupUi)
  }

    /**
     * Should be called immediately after calling the constructor
     * @param client // pass in the CinemaClient
     */
    setCinemaClient(client:VideoDemo) {
    this.videoDemo = client
    }
  
  updateVideoControls(bUseClientSelection:boolean=false){
    if (!this.videoDemo) {
        console.log("updateVideoControls called before videoControls has been set")
        return
    }
    if (this.catalogDisplayIndex >= this.videoDemo.films.length){
        console.log("updateVideoControls detects that index is "+this.catalogDisplayIndex+" but # films is "+ this.videoDemo.films.length)
        return
    }

    this.catalogDisplayIndex = this.videoDemo.loadedIndex

    if (this.catalogText && this.catalogText.length>0){
        if ((this.videoDemo.films != null) && (this.videoDemo.films.length > 0)) {
            this.catalogText = this.videoDemo.films[this.catalogDisplayIndex].title
                if (this.videoDemo.loadedIndex == this.catalogDisplayIndex){
                switch (this.videoDemo.videoState){
                    case VideoStates.PLAYING:
                        this.catalogText += "\n\nPlaying"
                        break
                    case VideoStates.PAUSED:
                        this.catalogText += "\n\nPaused"
                        break
                    case VideoStates.STOPPED:
                        this.catalogText += "\n\nStopped"
                        break
                    default:
                        break            }
            }
        }
        else {
            this.catalogText = "No catalog has been provided\nfor this theater."
        }
        if ((this.videoDemo.loadedIndex == this.catalogDisplayIndex)
             //&& (this.cinemaClient.videoState==CinemaTransportStates.PLAYING||this.cinemaClient.videoState==CinemaTransportStates.PAUSED)
            )
        {
            switch (this.videoDemo.videoState){
                case VideoStates.STOPPED:
                    this.catalogTextColor = Color4.Red()
                    break
                case VideoStates.PAUSED:
                    this.catalogTextColor = Color4.Yellow()
                    break
                case VideoStates.PLAYING:
                    this.catalogTextColor = Color4.Green()
                    break
                default:
                    break
            }
        }
        else {
            this.catalogTextColor = Color4.White()
        }
    }
    else {
        console.log("updateVideoControls called with this.catalogText UI element undefined")
    }
  }

    getCatalogText() {
        return this.catalogText
    }

    getCatalogTextColor() {
        return this.catalogTextColor
    }
    getCatalogDisplayIndex() {
        return this.catalogDisplayIndex
    }
    setCatalogDisplayIndex(n:number) {
        this.catalogDisplayIndex = n
    }
}


////////////////////////////////////////////////////////


export class VideoControls3D {
    titleDisplay:Entity = engine.addEntity()
    catalogText:string="Uninitialized"
    catalogTextColor:Color4 = Color4.White()
    catalogDisplayIndex:number = 0
    maximizedLabel:string = "UNINIT"
    videoDemo?:VideoDemo
  
    /**
     * Must be called immediately after calling the constructor
     * @param client // pass in the CinemaClient
     */
    setCinemaClient(client:VideoDemo) {
        this.videoDemo = client
    }

    constructor(public root:Entity)  {
        // Title
        Transform.create(this.titleDisplay, {
            parent:root,
            position: { x: 0, y: .01, z: -1 },
            scale: { x: 0.5, y: 0.5, z: 0.5 },
            rotation: Quaternion.fromEulerDegrees(90, 0, 0) 
        })
        let textShape = TextShape.create(this.titleDisplay)
        textShape.text = this.videoDemo?(this.videoDemo.films[this.catalogDisplayIndex].title):"UNINIT TITLE"

        // Stop button
        let stopButton = engine.addEntity()
        Transform.create(stopButton, {
        parent:root,
        position: { x: -0.3, y: 0.25, z: 0 },
        scale: { x: 0.5, y: 0.5, z: 0.5 },
        rotation: Quaternion.fromEulerDegrees(0, 0, 0) 
        })
        MeshRenderer.setBox(stopButton)
        MeshCollider.setBox(stopButton)
        Material.setBasicMaterial(stopButton,{
        diffuseColor:Color4.Red()
        })

        // Pause button
        let pauseButton = engine.addEntity()
        Transform.create(pauseButton, {
        parent:root,
        position: { x: 0.3, y: 0.25, z: 0 },
        scale: { x: 0.5, y: 0.5, z: 0.5 },
        rotation: Quaternion.fromEulerDegrees(0, 0, 0) 
        })
        MeshRenderer.setBox(pauseButton)
        MeshCollider.setBox(pauseButton)
        Material.setBasicMaterial(pauseButton,{
        diffuseColor:Color4.Yellow()
        })
        
        
        // Play button
        let playButton = engine.addEntity()
        Transform.create(playButton, {
        parent:root,
        position: { x: 0.9, y: 0.25, z: 0 },
        scale: { x: 0.5, y: 0.5, z: 0.5 },
        rotation: Quaternion.fromEulerDegrees(0, 0, 0) 
        })
        MeshRenderer.setBox(playButton)
        MeshCollider.setBox(playButton)
        Material.setBasicMaterial(playButton,{
        diffuseColor:Color4.Green()
        })

        this.updateVideoControls()

        // Stop Button
        pointerEventsSystem.onPointerDown(
        {
            entity: stopButton,
            opts: {
            button: InputAction.IA_POINTER,
            hoverText: 'Select Movie',
            maxDistance:32,
            showFeedback:false
            
            }
        },
        () => {
            if (this.videoDemo){
                this.videoDemo.stopVideo(true)
                console.log("\n*** after stopVideo call")
            }
            else {
                console.log("Stop button pressed before controls initialized")
            }
        }
        )

        // Pause button
        pointerEventsSystem.onPointerDown(
        {
            entity: pauseButton,
            opts: {
            button: InputAction.IA_POINTER,
            hoverText: 'Select Movie',
            maxDistance:32,
            showFeedback:false
            
            }
        },
        () => {
            // let videoPlayer = VideoPlayer.getMutable(videoPlayerEntity)
            // videoPlayer.playing = false
            if (this.videoDemo) {
                this.videoDemo.pauseVideo()
            }
            else {
                console.log("Pause button pressed before controls initialized")
            }
        }
        )

        // Play button
        pointerEventsSystem.onPointerDown(
        {
            entity: playButton,
            opts: {
            button: InputAction.IA_POINTER,
            hoverText: 'Select Movie',
            maxDistance:32,
            showFeedback:false
            
            }
        },
        () => {		
            if (this.videoDemo) {
                this.videoDemo.playVideo()
            }
            else {
                console.log("Play button pressed before controls initialized")
            }
        }
        )
    }
    
    setTitleText(text:string) {
        // for React UI:
        this.catalogText = text

        // for SimpleVideoControls
        let t = TextShape.getMutable(this.titleDisplay)
        t.text = text
    }
    
    
    setTitleColor(color:Color4){
        // for React UI:
        this.catalogTextColor = color
        
        // for SimpleVideoControls
        let t = TextShape.getMutable(this.titleDisplay)
        t.textColor = color
    
    }

    selectPreviousVideo() {
        if (this.catalogDisplayIndex > 0 ) {
            this.catalogDisplayIndex--
        }
        this.updateVideoControls()
    }

    selectNextVideo() {
        if (this.videoDemo){
            if (this.catalogDisplayIndex < this.videoDemo.films.length-1) {
                this.catalogDisplayIndex++
            }
            this.updateVideoControls()
        }
    }

    updateVideoControls(){
        let color:Color4

        if (this.videoDemo) {
            this.setTitleText(this.videoDemo.films[this.catalogDisplayIndex].title)
        
            if (this.catalogDisplayIndex != this.videoDemo.loadedIndex) {
    
                this.setTitleColor(Color4.White())
            }
            else {
                switch (this.videoDemo.videoState) {
                    case VideoStates.STOPPED:
                        this.setTitleColor(Color4.Red())
                        break
                    case VideoStates.PAUSED:
                        this.setTitleColor(Color4.Yellow())
                        break	
                    case VideoStates.PLAYING:
                        this.setTitleColor(Color4.Green())
                        break
                }
            }
        }
    }

    getMinimizedLabel(){
        return this.maximizedLabel
    }
    
    getMaximizedLabel(){
        return this.maximizedLabel
    }
    
    getCatalogTextColor() {
        return this.catalogTextColor
    }
    getCatalogDisplayIndex() {
        return this.catalogDisplayIndex
    }
    setCatalogDisplayIndex(n:number){
        this.catalogDisplayIndex = n
    }

    getCatalogText(){
        return "UNIT Catalog Text\nfrom SimpleVideoControls" // todo yyy implement
      }
    
}

export class VideoDemo {

    // This entity has no transform or shape (not located or visible) 
    // but will hold the VideoPlayer component.
    // It is NOT the same as the screen entity(s)
    videoPlayerEntity:Entity|undefined = undefined

    // There are the one or more screens in this.screens[]
    
    videoTexture?:TextureUnion
    videoPlayer?:PBVideoPlayer

    loadedIndex:number=0
    videoState = VideoStates.STOPPED
    
    constructor(
        public films:SimpleVideoSource[],
        public screens:Entity[],
        public videoControls3D?:VideoControls3D, 
        public videoControls2D?:VideoControls2D
    ) {
        // Since the videoControls (which implements IVideoControls hence its specific class unknown)
        // is typically instantiated before the CinemaClient,
        // set backlink from controls to client
        if (videoControls3D) {
            videoControls3D.setCinemaClient(this)
            videoControls3D.updateVideoControls() // do this once the setup is complete
        }
        if (videoControls2D) {
            videoControls2D.setCinemaClient(this)
            videoControls2D.updateVideoControls() // do this once the setup is complete
        }
    }
    
    updateUi(){
        if (this.videoControls3D) this.videoControls3D.updateVideoControls()
        if (this.videoControls2D) this.videoControls2D.updateVideoControls()
    }
        
    loadSelectedVideo(){
        console.log("LOAD")
        // create (or re-create) the videoPlayerEntity
        if (this.videoPlayerEntity) {
            let videoPlayer = VideoPlayer.getOrCreateMutable(this.videoPlayerEntity)
            videoPlayer.playing = false
        }
        this.videoPlayerEntity = engine.addEntity()
        if (this.videoControls3D) {
            this.loadedIndex = this.videoControls3D.getCatalogDisplayIndex()
        }
        else if (this.videoControls2D) {
            this.loadedIndex = this.videoControls2D.getCatalogDisplayIndex()
        }
        else {
            this.loadedIndex = 0
        }
        let vp = VideoPlayer.createOrReplace(this.videoPlayerEntity, {
            src: this.films[this.loadedIndex].src, 
            playing: false,
            volume: 1.0,
            position: 0
        })
    
        if (Material.has(this.screens[0])){
            let m = Material.get(this.screens[0])
            Material.deleteFrom(this.screens[0])
        }
        Material.setBasicMaterial(this.screens[0], {
            texture: Material.Texture.Video({ videoPlayerEntity: this.videoPlayerEntity })
        })
        let videoPlayer = VideoPlayer.getOrCreateMutable(this.videoPlayerEntity)
        videoPlayer.playing = false
        videoPlayer.src = this.films[this.loadedIndex].src
        videoPlayer.position = 0
        videoPlayer.volume = 1.0
        this.videoState = VideoStates.STOPPED
        this.updateUi()
    }
    
    playVideo() {
        console.log("PLAY")
        if (this.videoState == VideoStates.STOPPED) {
            this.loadSelectedVideo()
        }
        console.log("playVideo 2")
        if (!this.videoPlayerEntity) {
            return
        }
        let videoPlayer = VideoPlayer.getOrCreateMutable(this.videoPlayerEntity)
        videoPlayer.playing = true
        this.videoState = VideoStates.PLAYING
        this.updateUi()
    }
    
    pauseVideo() {
        console.log("PAUSE")
        if (!this.videoPlayerEntity) return
    
        if (this.videoState == VideoStates.PLAYING) {
            let videoPlayer = VideoPlayer.getOrCreateMutable(this.videoPlayerEntity)
            console.log("Pausing at position "+videoPlayer.position)
            videoPlayer.playing = false
            console.log("Paused at position "+videoPlayer.position)
            this.videoState = VideoStates.PAUSED
            this.updateUi()
        }
    }
    
    stopVideo(fireEvent:boolean){
        console.log("STOP")
        if (!this.videoPlayerEntity) return
        // this.loadSelectedVideo()
        let videoPlayer = VideoPlayer.getOrCreateMutable(this.videoPlayerEntity)
        videoPlayer.playing = false
        videoPlayer.position = 0
        this.videoState = VideoStates.STOPPED
        this.updateUi()
    }

}

export function setupVideoDemo(
    films:SimpleVideoSource[],
    screens:Entity[],
    videoControls3D?:VideoControls3D, 
    videoControls2D?:VideoControls2D
):VideoDemo
{
    // singleton instance global variable
    theVideoDemo = new VideoDemo(
        films,
        screens,
        videoControls3D,
        videoControls2D
    )
    return theVideoDemo
}


