set commitno=
for /f "delims=" %%a in ('git describe --tags --always') do @set commitno=%%a
echo %commitno%
echo export const dclbuildversion='%commitno%-%DATE%-%TIME%'>src\version.ts